// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": "0.0",
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

function init() {
    /** Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}

$(document).ready(function(){
    let edit = false;

    /**Se carga toda la lista de productos NO eliminados al abrir la página. */
    function mostrarLista(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function(response){
                let products = JSON.parse(response);
                let template = '';

                    products.forEach(product => {

                        let descripcion = '';
                        descripcion += '<li>precio: '+product.precio+'</li>';
                        descripcion += '<li>unidades: '+product.unidades+'</li>';
                        descripcion += '<li>modelo: '+product.modelo+'</li>';
                        descripcion += '<li>marca: '+product.marca+'</li>';
                        descripcion += '<li>detalles: '+product.detalles+'</li>';

                        template += `
                            <tr productId="${product.id}">
                                <td>${product.id}</td>
                                <td><a href="#" class = "product-item">${product.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td><button class="product-delete btn btn-danger"> Eliminar</button></td>
                            </tr>
                        `;
                    });
                $('#products').html(template);
            }
        })
    }
    // SE LISTAN TODOS LOS PRODUCTOS
    mostrarLista();

/** Cargar en la barra de estado los nombre de los productos NO eliminados coincidentes con lo que se 
 * “vaya” tecleando en el campo de búsqueda. */
    $('#product-result').hide();
    $('#search').keyup(function(e){
        if($('#search').val()){
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                type: 'POST',
                data: { search },
                success: function(response){

                    let products = JSON.parse(response);
                    let template = '';

                    let template_bar = '';
                    products.forEach(product => {
                         template_bar += `
                            <li>${product.nombre}</li>
                        `;
    /**Cargar en la tabla de productos NO eliminados que coincidan con lo que se “vaya” tecleando en el campo de búsqueda. */
                        let descripcion = '';
                        descripcion += '<li>precio: '+product.precio+'</li>';
                        descripcion += '<li>unidades: '+product.unidades+'</li>';
                        descripcion += '<li>modelo: '+product.modelo+'</li>';
                        descripcion += '<li>marca: '+product.marca+'</li>';
                        descripcion += '<li>detalles: '+product.detalles+'</li>';

                        template += `
                            <tr productId="${product.id}">
                                <td>${product.id}</td>
                                <td><a href="#" class = "product-item">${product.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td><button class="product-delete btn btn-danger"> Eliminar</button></td>
                            </tr>
                        `;
                    });
                $('#container').html(template_bar);
                $('#products').html(template);
                $('#product-result').show();
                }
            })
        }
    })

    $('#product-form').submit(function(e){
        e.preventDefault();

        var productoJsonString = document.getElementById('description').value;
        var finalJSON = JSON.parse(productoJsonString);
        finalJSON['nombre'] = document.getElementById('name').value;
        finalJSON['id'] = document.getElementById('productId').value;
        finalJSON['eliminado'] = document.getElementById('productEli').value;
        productoJsonString = JSON.stringify(finalJSON,null,2);

        let url =  edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
        console.log(url);

        function validar(){
            var producto = eval('(' +  productoJsonString + ')');  //Se evalúa el string y se obtiene un objeto JavaScript

            var valido = true;
            //Validación de expresiones regulares
            var alfanumerico = /^[A-Za-z0-9\-\ ]+$/g;
            var decimal =  /^\d*(\.\d{3})?\d{0,1}$/;
            var extension = /\.(jpg|png)$/i;
        
            /** PARTE "Nombre". El nombre debe ser requerido. */
                if(!producto.nombre || producto.nombre.length <= 4){
                    alert(' Nombre del producto no dado o muy corto.');
                    valido = false;
                /** El nombre debe tener 100 caracteres o menos. */
                } else if(producto.nombre.length > 100){
                    alert(' Nombre del producto muy largo.');
                    valido = false;
                }
        
            /** PARTE "Marca". La marca debe ser requerida y seleccionarse de una lista de opciones. */
                if(!producto.marca ){
                    alert('Ninguna marca dada.');
                    valido = false;
                }
        
            /** PARTE "Modelo". El modelo debe ser requerido. */
                if(!producto.modelo || producto.modelo.length <= 4){
                    alert('Nombre del modelo no dado o muy corto');
                    valido = false;
                } else if(producto.modelo.length > 25){  /** El modelo debe tener 25 caracteres o menos. */
                    alert('Nombre del modelo muy largo');
                    valido = false;
                /** El modelo debe ser tipo texto alfanumérico. Se permite el guión (-) y el espacio en blanco ( ) */
                } else if(!alfanumerico.test(producto.modelo)){
                    alert('El nombre del modelo con caracteres invalidos (no alfanuméricos).');
                    valido = false;
                }
            /** PARTE "Precio". El precio debe ser requerido y debe ser mayor a 99.99*/
                if(!producto.precio){
                    alert('Precio del producto no dado.');
                    valido = false;
                } else if(producto.precio <= 99.99){
                    alert('El precio debe ser mayor a 99.99');
                    valido = false;
                } else if(decimal.test(producto.precio)){
                    alert('El precio debe tener un " . " p.e: 200.49 ');
                    valido = false;
                }
        
            /** PARTE "Detalles". Los detalles del producto son opcionales. De usarse, deben tener menos de 250 caracteres.*/
                if(producto.detalles.length > 250){
                    alert('Detalles del producto muy extensos (máximo 250 caracteres).');
                    valido = false;
                }
        
            /** PARTE "Unidades". Las unidades deben ser requeridas y el número registrado debe ser mayor o igual a 0 */
                if(!producto.unidades){
                    alert('Número de unidades disponibles del producto no dado.');
                    valido = false;
                }else if(producto.unidades < 0){
                    alert('El número de unidades disponibles del producto debe ser igual o mayor a 0.');
                    valido = false;
                }
        
            /** PARTE "Imagen". La ruta de la imagen es opcional. En caso de no registrarse se debe usar la ruta de una imagen por defecto  */
                if(!producto.imagen){
                    producto.imagen.value = "img/imagen_ejem.png";
                } else if(!extension.test(producto.imagen)) {
                    alert('El archivo a adjuntar no es una imagen');
                    valido = false;
                }
            return valido;
        }
/**Enviar un estatus y un mensaje al registrar un producto; ya sea que la inserción sea exitosa o no */
        if(validar()){
            $('#product-result').hide();
            $.post(url, productoJsonString, function(response){
                console.log(response);
                let mensajes = JSON.parse(response);
                    let template_bar = '';

                    template_bar += `
                        <li style="list-style: none;">status: ${mensajes.status}</li>
                        <li style="list-style: none;">message: ${mensajes.message}</li>
                    `;
                    $('#container').html(template_bar);
                    $('#product-result').show();

    /**Cargar toda la lista de productos NO eliminados al presionar el botón de “Agregar Producto”, 
     * esto para visualizar inmediatamente la lista actualizada */
                mostrarLista();
            });
        }
    });

/**Cargar toda la lista de productos NO eliminados después presionar el botón “Eliminar”, esto para visualizar 
 * inmediatamente la lista actualizada. */
    $(document).on('click', '.product-delete', function(){
        if(confirm('¿Desea eliminar el producto seleccionad?')){
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');

            $.post('backend/product-delete.php', {id}, function(response){
                let mensajes = JSON.parse(response);
                    let template_bar = '';

                    template_bar += `
                        <li style="list-style: none;">status: ${mensajes.status}</li>
                        <li style="list-style: none;">message: ${mensajes.message}</li>
                    `;
                    $('#container').html(template_bar);
                    $('#product-result').show();
               mostrarLista();
            });
        }
    });

/** Funcionalidad de “edición”/“actualización”   */
    $(document).on('click', '.product-item', function(){
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');

        $.post('backend/product-single.php', {id}, function(response){
            const aux = JSON.parse(response);   //Se convierte el resultado en un string
            //Se crea un nuevo array con los datos necesarios a mostrar en el texarea
            var array = {
                "precio": aux.precio, 
                "unidades": aux.unidades, 
                "modelo": aux.modelo, 
                "marca": aux.marca, 
                "detalles": aux.detalles,
                "imagen": aux.imagen
            };

            var json = JSON.stringify(array,null,2);    //Se convierte array a formato json
            //Se llena el texarea
            $('#description').val(json);

            //Se llenan los datos no contemplados en el texarea
            $('#name').val(aux.nombre);     
            $('#productId').val(aux.id);
            $('#productEli').val(aux.eliminado);

            edit = true;
        })
    });
});