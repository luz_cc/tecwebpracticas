<?php
    use API_Lucero\Update\Actualizar as Actualizar;
    require_once __DIR__ . '/../vendor/autoload.php';

    $edit = new Actualizar();
    $edit->Actualizar('marketzone');

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');

    $edit->edit($producto);
    echo $edit->getResponse();
?>