<?php
    use API_Lucero\Read\Leer as Leer;
    require_once __DIR__ . '/../vendor/autoload.php';

    $single = new Leer();
    $single->Leer('marketzone');

    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        $single->single($id);
        echo $single->getResponse();
    }  
?>