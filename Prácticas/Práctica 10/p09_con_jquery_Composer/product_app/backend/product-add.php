<?php
    use API_Lucero\Create\Crear as Crear;
    require_once __DIR__ . '/../vendor/autoload.php';
    
    $add = new Crear();
    $add->Crear('marketzone');

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');

    $add->add($producto);
    echo $add->getResponse();
?>