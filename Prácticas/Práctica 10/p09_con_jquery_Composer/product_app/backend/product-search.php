<?php
    use API_Lucero\Read\Leer as Leer;
    require_once __DIR__ . '/../vendor/autoload.php';

    $search = new Leer();
    $search->Leer('marketzone');

    if( isset($_POST['search']) ) {
        $cadena = $_POST['search'];
        $search->search($cadena);
        echo $search->getResponse();
    }
?>