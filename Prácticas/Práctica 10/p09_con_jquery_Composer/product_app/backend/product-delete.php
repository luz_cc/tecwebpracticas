<?php
    use API_Lucero\Delete\Eliminar as Eliminar;
    require_once __DIR__ . '/../vendor/autoload.php';

    $delete = new Eliminar();
    $delete->Eliminar('marketzone');

    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        $delete->delete($id);
        echo $delete->getResponse();
    }
?>