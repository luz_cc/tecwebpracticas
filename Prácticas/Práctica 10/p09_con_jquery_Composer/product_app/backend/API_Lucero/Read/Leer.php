<?php
    namespace API_Lucero\Read;

    use API_Lucero\DataBase as DataBase;
    require_once __DIR__ . '/../DataBase.php';

    class Leer extends DataBase{

        public function Leer($string){
            parent::DataBase($string);
            parent::getResponse();
        }

        public function list(){
            $result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0");
            $rows = $result->fetch_all(MYSQLI_ASSOC);

            if(!is_null($rows)) {

                foreach($rows as $num => $row) {
                    foreach($row as $key => $value) {
                        $this->response[$num][$key] = utf8_encode($value);
                    }
                }
            }
        }

        public function search($cadena){

            if($result = $this->conexion->query("SELECT * FROM productos WHERE (id = '{$cadena}' OR nombre LIKE '%{$cadena}%' OR marca LIKE '%{$cadena}%' OR detalles LIKE '%{$cadena}') AND eliminado = 0")){
                unset ($this->response[0]);
                $rows = $result->fetch_all(MYSQLI_ASSOC);

                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
            }
        }

        public function single($id){

            if($result = $this->conexion->query("SELECT * FROM productos WHERE id = '{$id}'")){
                unset ($this->response[0]);
                $this->response = $result->fetch_array(MYSQLI_ASSOC);
            }
        }
    }
?>