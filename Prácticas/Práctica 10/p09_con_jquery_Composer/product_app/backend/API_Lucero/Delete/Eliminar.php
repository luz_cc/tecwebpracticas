<?php
    namespace API_Lucero\Delete;

    use API_Lucero\DataBase as DataBase;
    require_once __DIR__ . '/../DataBase.php';

    class Eliminar extends DataBase{

        public function Eliminar($string){
            parent::DataBase($string);
            parent::getResponse();
        }

        public function delete($id_d){
            $result = $this->conexion->query("UPDATE productos SET eliminado = 1 WHERE id = {$id_d}");
            unset ($this->response[0]);

            if($result){
                $this->response['status'] =  "success";
                $this->response['message'] =  "Producto eliminado";
            } else {
                $this->response['message'] = "ERROR: No se ejecutó la sentencia query";
            }
        }
    }
?>