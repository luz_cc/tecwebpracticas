<?php
    namespace API_Lucero\Update;

    use API_Lucero\DataBase as DataBase;
    require_once __DIR__ . '/../DataBase.php';

    class Actualizar extends DataBase{

        public function Actualizar($string){
            parent::DataBase($string);
            parent::getResponse();
        }

        public function edit($objeto){
            $jsonOBJ = json_decode($objeto);

            //Acceso a datos del producto
            $idP          = $jsonOBJ->id;
            $nombreP      = $jsonOBJ->nombre;
            $marcaP       = $jsonOBJ->marca;
            $modeloP      = $jsonOBJ->modelo;
            $precioP      = $jsonOBJ->precio;
            $detallesP    = $jsonOBJ->detalles;
            $unidadesP    = $jsonOBJ->unidades;
            $imagenP      = $jsonOBJ->imagen;

            unset ($this->response[0]);
            if($this->conexion->query("SELECT * FROM productos WHERE id = '$idP'")){
                if($this->conexion->query("UPDATE productos SET nombre= '$nombreP', marca= '$marcaP', modelo= '$modeloP', precio= '$precioP', detalles= '$detallesP', unidades= '$unidadesP', imagen= '$imagenP' WHERE id= $idP")){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Datos actualizados";
                }
                else{
                    $this->response['message'] = "ERROR: No se ejecutó la sentencia query";
                }
            }
        }
    }
?>