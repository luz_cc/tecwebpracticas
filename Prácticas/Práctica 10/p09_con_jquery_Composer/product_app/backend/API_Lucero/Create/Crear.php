<?php
    namespace API_Lucero\Create;

    use API_Lucero\DataBase as DataBase;
    require_once __DIR__ . '/../DataBase.php';

    class Crear extends DataBase{

        public function Crear($string){
            parent::DataBase($string);
            parent::getResponse();
        }

        public function add($objeto){
            $jsonOBJ = json_decode($objeto);
            
            $result = $this->conexion->query("SELECT * FROM productos WHERE nombre = '{$jsonOBJ->nombre}' AND eliminado = 0");

            if ($result->num_rows == 0) {
                $this->conexion->set_charset("utf8");
                unset ($this->response[0]);
                
                if($this->conexion->query("INSERT INTO productos VALUES (null, '{$jsonOBJ->nombre}', '{$jsonOBJ->marca}', '{$jsonOBJ->modelo}', {$jsonOBJ->precio}, '{$jsonOBJ->detalles}', {$jsonOBJ->unidades}, '{$jsonOBJ->imagen}', 0)")){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto agregado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecutó la sentencia query";
                }
            }
        }
    }
?>