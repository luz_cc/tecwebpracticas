// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": "0.0",
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarProducto(e) {
    /**
     * Revisar la siguiente información para entender porqué usar event.preventDefault();
     * http://qbit.com.mx/blog/2013/01/07/la-diferencia-entre-return-false-preventdefault-y-stoppropagation-en-jquery/#:~:text=PreventDefault()%20se%20utiliza%20para,escuche%20a%20trav%C3%A9s%20del%20DOM
     * https://www.geeksforgeeks.org/when-to-use-preventdefault-vs-return-false-in-javascript/
     */
    e.preventDefault();

    // SE OBTIENE EL DATO A BUSCAR
    var buscar = document.getElementById('search').value;

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function () {

        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n'+client.responseText);
            
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText);    // similar a eval('('+client.responseText+')');
            
            var cuenta = Object.keys(productos).length;

            //Imprime en consola en número de objetos del array 
            console.log('El array tiene '+ cuenta + ' registros');

            // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
            if (!cuenta){
                alert('No se encontraron registros');
            }
            else{ 
                for (i of productos) {

                    // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                        let descripcion = '';
                        descripcion += '<li>precio: '+i.precio+'</li>';
                        descripcion += '<li>unidades: '+i.unidades+'</li>';
                        descripcion += '<li>modelo: '+i.modelo+'</li>';
                        descripcion += '<li>marca: '+i.marca+'</li>';
                        descripcion += '<li>detalles: '+i.detalles+'</li>';

                    // SE CREA UNA PLANTILLA PARA CREAR LA(S) FILA(S) A INSERTAR EN EL DOCUMENTO HTML
                    let template = '';
                        template += `
                            <tr>
                                <td>${i.id}</td>
                                <td>${i.nombre}</td>
                                <td><ul>${descripcion}</ul></td>
                            </tr>
                            `;

                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    document.getElementById("productos").innerHTML += template;
                }
            }
        }
    };
    client.send("buscar="+buscar);  //Se envía el dato a buscar

    //Limpiar el resultado anterior
    document.getElementById("productos").innerHTML = ' ';
}

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();

    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;

    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);

    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;

    // SE OBTIENE EL STRING DEL JSON FINAL
    productoJsonString = JSON.stringify(finalJSON,null,2);

    function validar(){
        //Se evalúa el string y se obtiene un objeto JavaScript
        var producto = eval('(' +  productoJsonString + ')');
    
        var valido = true;
    
        //Validación de expresiones regulares
        var alfanumerico = /^[A-Za-z0-9\-\ ]+$/g;
        var decimal =  /^\d*(\.\d{3})?\d{0,1}$/;
        var extension = /\.(jpg|png)$/i;
    
    
        /** PARTE "Nombre" 
            * El nombre debe ser requerido. */
            if(!producto.nombre || producto.nombre.length <= 4){
                alert(' Nombre del producto no dado o muy corto.');
                valido = false;
            /** El nombre debe tener 100 caracteres o menos. */
            } else if(producto.nombre.length > 100){
                alert(' Nombre del producto muy largo.');
                valido = false;
            }
    
        /** PARTE "Marca" 
            * La marca debe ser requerida y seleccionarse de una lista de opciones. */
            if(!producto.marca ){
                alert('Ninguna marca dada.');
                valido = false;
            }
    
        /** PARTE "Modelo" 
            * El modelo debe ser requerido. */
            if(!producto.modelo || producto.modelo.length <= 4){
                alert('Nombre del modelo no dado o muy corto');
                valido = false;
            /** El modelo debe tener 25 caracteres o menos. */
            } else if(producto.modelo.length > 25){
                alert('Nombre del modelo muy largo');
                valido = false;
            /** El modelo debe ser tipo texto alfanumérico.
                Se permite el guión (-) y el espacio en blanco ( ) */
            } else if(!alfanumerico.test(producto.modelo)){
                alert('El nombre del modelo con caracteres invalidos (no alfanuméricos).');
                valido = false;
            }
        /** PARTE "Precio" 
        * El precio debe ser requerido y debe ser mayor a 99.99*/
            if(!producto.precio){
                alert('Precio del producto no dado.');
                valido = false;
            } else if(producto.precio <= 99.99){
                alert('El precio debe ser mayor a 99.99');
                valido = false;
            } else if(decimal.test(producto.precio)){
                alert('El precio debe tener un " . " p.e: 200.49 ');
                valido = false;
            }
    
        /** PARTE "Detalles" 
            * Los detalles del producto son opcionales. 
            * De usarse, deben tener 250 caracteres o menos.*/
            if(producto.detalles.length > 250){
                alert('Detalles del producto muy extensos (máximo 250 caracteres).');
                valido = false;
            }
    
        /** PARTE "Unidades" 
            * Las unidades deben ser requeridas y el número registrado debe ser mayor o igual a 0 */
            if(!producto.unidades){
                alert('Número de unidades disponibles del producto no dado.');
                valido = false;
            }else if(producto.unidades < 0){
                alert('El número de unidades disponibles del producto debe ser igual o mayor a 0.');
                valido = false;
            }
    
        /** PARTE "Imagen" 
            * La ruta de la imagen es opcional.
            * En caso de no registrarse se debe usar la ruta de una imagen por defecto  */
            if(!producto.imagen){
                producto.imagen.value = "img/imagen_ejem.png";
            } else if(!extension.test(producto.imagen)) {
                alert('El archivo a adjuntar no es una imagen');
                valido = false;
            }

        return valido;
    }

    if(validar()){

         // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
        var client = getXMLHttpRequest();

        client.open('POST', './backend/create.php', true);
        client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
        client.onreadystatechange = function () {
        
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            //console.log(client.responseText);

            var respuesta = client.responseText;

            //Se muestra en una ventana de alerta la respuesta
             alert(respuesta);
            }
        };
        client.send(productoJsonString);  //Se envía el producto a registrar
    }
}


// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try{
        objetoAjax = new XMLHttpRequest();
    }catch(err1){
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try{
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(err2){
            try{
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(err3){
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}