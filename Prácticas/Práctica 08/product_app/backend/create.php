<?php
    include_once __DIR__.'/database.php';   //Conexión a base de datos

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');

    if(!empty($producto)) {
        // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto);
        
        //Acceso a datos del producto
        $nombreP      = $jsonOBJ->nombre;
        $marcaP       = $jsonOBJ->marca;
        $modeloP      = $jsonOBJ->modelo;
        $precioP      = $jsonOBJ->precio;
        $detallesP    = $jsonOBJ->detalles;
        $unidadesP    = $jsonOBJ->unidades;
        $imagenP      = $jsonOBJ->imagen;

    /** Se hace una consulta a la tabla de registros de la base de datos 
        * para comprobar si el registro ya existe */
        $consult = "SELECT * FROM productos WHERE nombre = '{$nombreP}' AND eliminado = 0";

        $result = $conexion->query($consult);

        //Se comprueba si el número de líneas devueltas de la consulta es igual a 1

        if(mysqli_num_rows($result) == 1){ 
            //En caso positivo se indica que el registro ya existe
            echo "ERROR \nEl producto ya existe en la base de datos";
        
        } else {
            // En caso contrario el registro se inserta a la base de datos
            $insert = "INSERT INTO productos VALUES (null, '{$nombreP}', '{$marcaP}', '{$modeloP}', {$precioP}, '{$detallesP}', {$unidadesP}, '{$imagenP}',0)";
            
           if($conexion->query($insert)){
                 $conexion->insert_id;
                echo "Producto registrado con éxito";
            } else {
                echo "No se pudo registrar el producto" ;
            }
        }

        $conexion->close();
    }
?>