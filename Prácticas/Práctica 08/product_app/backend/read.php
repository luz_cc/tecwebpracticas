<?php
     //Conexión a la base de datos
    include_once __DIR__.'/database.php';
    
    //Designa el contenido en formato JSON, codificado en la codificación de caracteres UTF-8.
    //header('Content-Type: application/json; charset=UTF-8'); 

    // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
    $data = array();

    // SE VERIFICA HABER RECIBIDO EL DATO A BUSCAR YA SEA UN ID, UNA PALABRA O VARIAS PALABRAS
    if (isset($_POST['buscar'])){
        $buscar = $_POST['buscar'];
        // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
        //echo $buscar;
         
        if ( $result = $conexion->query("SELECT * FROM productos WHERE id = '{$buscar}' or nombre LIKE '%$buscar%' or marca LIKE '%$buscar%' or detalles LIKE '%$buscar'") ) {
            /** Se extraen las tuplas obtenidas de la consulta */
            $row = $result->fetch_all(MYSQLI_ASSOC);

            /** Se crea un arreglo con la estructura deseada */
            foreach($row as $num => $registro) {            // Se recorren tuplas
                foreach($registro as $key => $value) {      // Se recorren campos
                    $data[$num][$key] = utf8_encode($value);
                }
            }
            $result->free();
        }
        //Se cierra la conexión
        $conexion->close();
    }
    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>