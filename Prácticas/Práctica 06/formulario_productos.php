<!DOCTYPE html >
<html>

  <head>
    <meta charset="utf-8" >
    <title>Registro de productos</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>
  </head>

  <body>
    <h1 style="text-align:center;">&ldquo;Marketzone&rdquo;</h1>
    <h1 style="text-align:center;">Registro de Productos</h1>

    <p>Completa los siguientes campos para poder registrar un producto a la base de datos.</p>
    
    <form id="formulario" action="http://localhost/tecnologiasweb/practicas/p06/set_producto_v2.php" method="post">

        <h2>Información general</h2>

        <fieldset>
            <ul>
            <!--Parte "Nombre"--> 
                <li><label for="form-name">Nombre:</label>
                    <input type="text" name="name" id="form-name" placeholder="Termo digital">
                </li>
                <!--Parte "Marca"--> 
                <li><label for="form-marca">Marca:</label>
                    <input type="text" name="marca" id="form-marca" placeholder="VAGABOX">
                </li>
                <!--Parte "Modelo"--> 
                <li><label for="form-model">Modelo:</label>
                    <input type="text" name="model" id="form-model" placeholder="MX-890">
                </li>
                    
                <li><label for="form-price">Precio: ($)</label>
                    <input step="any" type="number" name="price" id="form-price"placeholder="189.50">
                </li>
                <!--Parte "Detalles"--> 
                <li><label for="form-details">Detalles del producto:</label><br>
                    <textarea name="details" rows="4" cols="60" id="form-details" placeholder="No más de 250 caracteres de longitud"></textarea>
                </li>
                <!--Parte "Unidades"--> 
                <li><label for="form-units">Unidades:</label>
                    <input type="number" name="units" id="form-units" placeholder="590">
                </li>
                <!--Parte "Imagen"--> 
                <li><label for="form-img">Imagen del producto(ruta): </label>
                    <input type="text" name="img" id="form-img" placeholder="img/imagen.png"/>
                </li>
            </ul>
        </fieldset>

        <p>
            <input type="submit" value="Registrar producto">
            <input type="reset">
        </p>

    </form>

    <h3>Elaborado por: <br> Cota Cagal Lucero </h3>
    
  </body>
</html>