<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <?php
        $nombre      = $_POST['name'];
        $marca       = $_POST['marca'];
        $modelo      = $_POST['model'];
        $precio      = $_POST['price'];
        $detalles    = $_POST['details'];
        $unidades    = $_POST['units'];
        $imagen      = $_POST['img'];
        $correcText  = true;
        $correctUnit = true;
        $flotante    = true;
        $extend      = true;

        /** Ningún campo de texto vacío */
        if($nombre and $marca and $modelo and $detalles){ 
            /** Ningún campo de texto menor a 4 caracteres */
            if(strlen($nombre) <= 4 and strlen($marca) <= 4 and strlen($modelo) <= 4 and strlen($detalles) <= 4 || strlen($detalles)>250){
                $correcText = false;
            }
            else{
                $correcText = true;
            }
        }
        else{ 
            $correcText = false;
        }

        /** La imagen debe tener una extesión */
        if($imagen){ 
            for($i=0; $i<strlen($imagen); $i++){
                if($imagen[$i] == "."){ 
                    $extend = true; 
                    break;
                }
                else{ 
                    $extend = false;
                }
            }
        
        }

        /** Campo de 'Unidades' tiene que ser mayor a 0 */
        if($unidades > 0){ $correctUnit = true; } else { $correctUnit = false; }

        /** Campo de 'Precio' tiene que ser mayor a 0 */
        if($precio){ 
            /** El precio tiene que ser un valor flotante, es decir, se debe identificar con un .  p.e $140.00*/
            for($i=0; $i<strlen($precio); $i++){
                if($precio[$i] == "."){ 
                    $flotante = true; 
                    break;
                }
                else{ 
                    $flotante = false;
                }
            }
        }
        else{
            $flotante = false;
        }

        /** SE CREA EL OBJETO DE CONEXION */
        @$link = new mysqli('localhost', 'root', 'akuatsu1221', 'marketzone');	

        /** comprobar la conexión */
        if ($link->connect_errno) {
            die('Falló la conexión: '.$link->connect_error.'<br/>');
            /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
        }

        /** Crear una tabla que no devuelve un conjunto de resultados */

        if($correcText == true and $correctUnit == true and $flotante == true and $extend == true){
            $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}',0)";
            if ($link->query($sql)){
                $link->insert_id;
            }
            else{
                echo 'El Producto no pudo ser insertado =(';
            }
        }

        $link->close();
    ?>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 20px; 
			background-color: #C4DF9B;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}
			h1 {color: #005825;
			border-bottom: 1px solid #005825;}
			h2 {font-size: 1.2em;
			color: #4A0048;}
            li {list-style: none;}
		</style>
	</head>
	<body>
        <h1>Resultados de registro de producto</h1>
        <?php if($correcText == true and $correctUnit == true and $flotante == true and $extend == true ){ ?>
            <h2>Producto registrado: Resumen de datos</h2>    

            <ul>
                <li><strong>Nombre:</strong> <em><?= $_POST['name']; ?></em></li>
                <li><strong>Marca:</strong> <em><?= $_POST['marca']; ?></em></li>
                <li><strong>Modelo:</strong> <em><?= $_POST['model']; ?></em></li>
                <li><strong>Precio:</strong> <em><?= $_POST['price']; ?></em></li>
                <li><strong>Detalles:</strong> <em><?= $_POST['details']; ?></em></li>
                <li><strong>Unidades:</strong> <em><?= $_POST['units']; ?></em></li>
                <li><strong>Imagen (ruta):</strong> <em><?= $_POST['img']; ?></em></li>
            </ul>
        <?php }
                elseif($correcText == false || $correctUnit == false || $flotante == false || $extend == false){?>

                <h2>Se han encontrado los siguientes errores</h2>
            
            <ul>
                <!--Parte "Nombre"--> 
                <?php if(!$nombre) {?>
                    <li><strong> Nombre </strong> del producto no dado. </li>
                <?php } elseif(strlen($nombre) <= 4 ) { ?>
                    <li><strong> Nombre </strong> de producto muy corto. </li>
                <?php } ?>

                <!--Parte "Marca"--> 
                <?php if(!$marca){ ?> 
                    <li> Nombre de la <strong>marca</strong> del producto no dado. </li>
                <?php } elseif(strlen($marca) <= 4 ) { ?>
                    <li>Nombre de la <strong> marca </strong> del producto muy corto. </li>
                <?php } ?>

                <!--Parte "Modelo"-->
                <?php if(!$modelo){ ?> 
                    <li><strong>Modelo</strong> del producto no dado. </li> 
                <?php } elseif(strlen($modelo) <= 4 ) { ?>
                    <li><strong>Modelo</strong> del producto muy corto. </li>
                <?php } ?> 

                <!--Parte "Precio"-->
                <?php if(!$precio){?> 
                    <li><strong>Precio</strong> del producto no nado. </li>
                <?php } elseif($flotante == false) { ?>
                    <li> Sintaxis del <strong>precio</strong> erróneo. Revise que el precio ingresado cuente con '.' (punto separador)</li> 
                <?php } ?>

                <!--Parte "Detalles"--> 
                <?php if(!$detalles){ ?> 
                    <li><strong> Detalles</strong> del producto no dados. </li> 
                <?php } elseif(strlen($detalles) <= 4 ) { ?>
                    <li><strong> Detalles</strong> del producto muy cortos. </li>
                <?php }elseif(strlen($detalles) > 250 ) { ?>
                    <li><strong> Detalles</strong> del producto muy extensos. </li>
                <?php } ?>

                <!--Parte "Unidades"-->                 
                <?php if(!$unidades || $unidades == 0){?> 
                    <li> Número de <strong>unidades</strong> del producto no dado. </li> 
                <?php } elseif($correctUnit== false) {?>
                    <li> El número de <strong>unidades</strong> es menor a 0 </li>
                <?php } ?>

                <!--Parte "Imagen"--> 
                <?php if(!$imagen){ ?>
                    <li> Ruta de la <strong>imagen</strong> del producto no dada. </li> 
                <?php } elseif($extend == false) {?>
                    <li> Entensión de la <strong>imagen</strong> no detectada (.png , .jpg, etc.). </li>
                <?php } ?>
            </ul>

            <?php } ?>

            <p>
                <a href="http://validator.w3.org/check?uri=referer"><img
                src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
            </p>
	</body>
</html>