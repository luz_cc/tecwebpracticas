<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN”
“http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> Práctica 3 – Manejo de variables en PHP </title>
    </head>
    <body>
        <h2><p style="text-align:center"> Práctica 3 – Manejo de variables en PHP </p></h2>
        <h3> Elaborado por: <br />
            Cota Cagal Lucero
        </h3>

        <?php
            echo "<h2> Ejercicio 1 </h2>";

            $_myvar = '$_myvar';
            $_7var = '$_7var';
        //    myvar = 'myvar'
            $myvar = '$myvar';
            $var7 = '$var7';
            $_element1 = '$_element1';
        //    $house*5 = 'o';
            echo "<p> <font size= 4> 1. <b> $_myvar </b> es válida. Está representado con el signo $, el 
            nombre comienza con un underscore (_) y contiene caracteres válidos del abecedario.</font></p>";

            echo "<p> <font size= 4> 2. <b> $_7var </b> es válida. Está representado con el signo $, el 
            nombre comienza con un underscore (_) y contiene caracteres válidos alfanuméricos. </font></p>";

            echo "<p> <font size= 4> 3. <b> myvar </b> es inválida. No está representado con el signo $ </font></p>";

            echo "<p> <font size= 4> 4. <b> $myvar </b> es válida. Está representado con el signo $, el 
            nombre comienza con una letra del abecedario y contiene caracteres válidos del abecedario. </font></p>";

            echo "<p> <font size= 4> 5. <b> $var7 </b> es válida. Está representado con el signo $, el 
            nombre comienza con una letra del abecedario y contiene caracteres válidos alfanuméricos. </font></p>";

            echo "<p> <font size= 4> 6. <b> $_element1 </b> es válida. Está representado con el signo $, el 
            nombre comienza con un underscore (_) y contiene caracteres válidos alfanuméricos. </font></p>";

            echo "<p> <font size= 4> 7. <b> $ house*5 </b> es inválida. El caracter * tiene un valor ASCII de 42. 
            42 está fuera del rango permitido (127-255). </font></p>";

            unset($_myvar, $_7var, $myvar, $var7, $_element1);  //Destrucción de variables

#######################################################################################################################

            echo "<h2> Ejercicio 2 </h2>";

            $a = "ManejadorSQL";
            $b = 'MySQL';
            $c = &$a;

            echo "<p> <font size= 4> a) Contenido de cada variable: </font></p>";
            echo "<pre>Variable <em><b>a</b></em>: ", $a, "</pre>";
            echo "<pre>Variable <em><b>b</b></em>: ", $b, "</pre>";
            echo "<pre>Variable <em><b>c</b></em>: ", $c, "</pre>";
            echo "<p> <font size= 4> b) Nuevas asignaciones. </font></p>";

            $a = "PHP server";
            $b = &$a;

            echo "<p> <font size= 4> c) Contenido de cada variable:</font></p>";
            echo "<pre>Variable <em><b>a</b></em>: ", $a, "</pre>";
            echo "<pre>Variable <em><b>b</b></em>: ", $b, "</pre>";
            echo "<pre>Variable <em><b>c</b></em>: ", $c, "</pre>";

            echo "<p> <font size= 4> d) Para comenzar, en el inciso a) a la variable <em>a</em> se le asigna el 
                  valor de <b>ManejadorSQL</b> y a <em>b</em> <b>MySQL</b>, pero <em>c</em> hace una referencia a 
                  la variable <em>a</em>, lo que da como resultado que lo asignado a <em>c</em> sea 
                  lo mismo que <em>a</em>, es decir, <b>ManejadorSQL</b>. Posteriormente, en el inciso b) 
                  se realiza un segundo bloque de asignaciones a las variables. El contenido de <em>a</em> 
                  pasa de <b>ManejadorSQL</b> a <b>$a</b>, la variable <em>b</em> hace una referencia a 
                  <em>a</em> y <em>c</em> se queda sin modificaciones. Ya que <em>b</em> y <em>c</em> hacen
                  referencia a <em>a</em>, entonces el contenido de las tres variables es <b>$a</b>. 
                  Esto se comprueba en el inciso c) </font></p>";

            unset($a, $b, $c);      //Destrucción de variables
        ?>

        <?php
            echo "<h2> Ejercicio 3 </h2>";

        #################################################################
            echo "<p> <font size= 4> 1. Primera asignación: <em> a, z, b, c </em> </font></p>";

            $a = "PHP5";
                echo "<pre>Variable <em><b>a</b></em>: ", var_dump($a), "</pre>";
            $z[] = &$a;
                echo "<pre>Arreglo <em><b>z</b></em>: ", var_dump($z), "</pre>";
            $b = "5a version de PHP";
                echo "<pre>Variable <em><b>b</b></em>: ", var_dump($b), "</pre>";

            /*Nota: El manejar la expresión $c = $b*10; podría provocar el mensaje: 
                        Warning: A non-numeric value encountered in ...
            Sin embargo, la operación se realizará sin invonvenientes. 
            Una alternativa para evitar el warning sería usar la expresión:
            $c = (int)$b*10;    */

            $c = $b*10;
                echo "<pre>Variable <em><b>c</b></em>: ", var_dump($c), "</pre>";

        #################################################################
            echo "<p> <font size= 4> 2. Segunda asignación: <em>a, b </em> </font></p>";

            $a .= $b;
                echo "<pre>Variable <em><b>a</b></em>: ", var_dump($a), "</pre>";
                echo "<pre>Arreglo <em><b>z</b></em>: ", var_dump($z), "</pre>";

            /*Nota: El manejar la expresión $b *= $c; podría provocar el mensaje: 
                        Warning: A non-numeric value encountered in ...
            Sin embargo, la operación se realizará sin invonvenientes. 
            Una alternativa para evitar el warning sería usar la expresión:
            $b = (int)$b*$c;    */

            $b *= $c;
                echo "<pre>Variable <em><b>b</b></em>: ", var_dump($b), "</pre>";
                echo "<pre>Variable <em><b>c</b></em>: ", var_dump($c), "</pre>";

        #################################################################
            echo "<p> <font size= 4> 3. Tercera asignación: <em>z</em> </font></p>";

            $z[0] = "MySQL";
                echo "<pre>Variable <em><b>a</b></em>: ", var_dump($a), "</pre>";
                echo "<pre>Arreglo <em><b>z</b></em>: ", var_dump($z), "</pre>";
                echo "<pre>Variable <em><b>b</b></em>: ", var_dump($b), "</pre>";
                echo "<pre>Variable <em><b>c</b></em>: ", var_dump($c), "</pre>";
                
            unset($a, $b, $c, $z);      //Destrucción de variables
        ?>

        <?php
            echo "<h2> Ejercicio 4 </h2>";

        #################################################################
            echo "<p> <font size= 4> 1. Primera asignación: <em> a, z, b, c </em> </font></p>";

            $a = "PHP5";
            $z[] = &$a;
            $b = "5a version de PHP";
            $c = $b*10;

            function mostrar(){
                global $a, $z, $b, $c;
                echo "<pre>Variable <em><b>a</b></em>: ", var_dump($a), "</pre>";
                echo "<pre>Arreglo <em><b>z</b></em>: ", var_dump($z), "</pre>";
                echo "<pre>Variable <em><b>b</b></em>: ", var_dump($b), "</pre>";
                echo "<pre>Variable <em><b>c</b></em>: ", var_dump($c), "</pre>";
            }
            mostrar();

        #################################################################
            echo "<p> <font size= 4> 2. Segunda asignación: <em> a, b </em>  </font></p>";

            $a .= $b;
            $b*= $c;

            mostrar();

        #################################################################
            echo "<p> <font size= 4> 3. Tercera asignación: <em>z</em>  </font></p>";

            $z[0] = "MySQL";
            
            mostrar();

            unset($a, $b, $c, $z);
        ?>

        <?php
            echo "<h2> Ejercicio 5 </h2>";

            $a = "7 personas";
            $b = (integer) $a;

            echo "<p> <font size= 4> a) Contenido de cada variable: </font></p>";
            echo "<pre>Variable <em><b>a</b></em>: ", $a, "</pre>";
            echo "<pre>Variable <em><b>b</b></em>: ", $b, "</pre>";

            echo "<p> <font size= 4> b) Nuevas asignaciones. </font></p>";
            $a = "9E3";
            $c = (double) $a;

            echo "<p> <font size= 4> a) Contenido de cada variable: </font></p>";
            echo "<pre>Variable <em><b>a</b></em>: ", $a, "</pre>";
            echo "<pre>Variable <em><b>b</b></em>: ", $b, "</pre>";
            echo "<pre>Variable <em><b>c</b></em>: ", $c, "</pre>";

            unset($a, $b, $c);
        ?>
        
        <?php
            echo "<h2> Ejercicio 6 </h2>";

            $a = "0";
            $b = "TRUE";
            $c = FALSE;
            $d = ($a OR $b);
            $e = ($a AND $c);
            $f = ($a XOR $b);
        

            echo "<p> <font size= 4> Valor booleano de cada variable (impresión con var_dump): </font></p>";
            echo "<pre>", var_dump(boolval($a), boolval($b), boolval($c), boolval($d), boolval($c),
            boolval($d), boolval($e), boolval($f)), "</pre>";

            echo "<p> <font size= 4> Valor booleano de <em>c</em> y <em>e</em> (impresión con echo):</font></p>";
        
        #################################################################
            echo "<em>Opción 1. Conversión a (int) con 0 = False y 1 = True </em>";
            echo '<br /> c: '.(int)$c;
            echo '<br /> e: '.(int)$e. '<br />';
        
        #################################################################
            echo "<br /> <em>Opción 2. Uso de var_export </em>";
            echo "<br />c. ", var_export($c);
            echo "<br />e. ",var_export($e). '<br />';
        
        #################################################################
            echo "<br /> <em>Opción 3. Uso de boolval </em>";
            echo '<br /> c: '.(boolval($c) ? 'true' : 'false');
            echo '<br /> e: '.(boolval($e) ? 'true' : 'false');

            unset($a, $b, $c, $d, $e, $f);
        ?>

        <?php
            echo "<h2> Ejercicio 7 </h2>";
        
        #################################################################
            echo "<p> <font size= 4> a. Versión de Apache y PHP </font></p>";
            echo "<b>". $_SERVER['SERVER_SOFTWARE']. "</b>";
        
        #################################################################
            echo "<p> <font size= 4> b. Nombre del sistema operativo (servidor) </font></p>";
            echo "Nombre del servidor: <b>". $_SERVER['SERVER_NAME']. "</b> <br />";

            echo "Información sobre el User Agent: <b>".$_SERVER['HTTP_USER_AGENT']. "</b> <br />";
            echo "Plataforma en la que se ejecuta el User Agent: <b>".$_SERVER['HTTP_SEC_CH_UA_PLATFORM']. "</b> <br />";
 
        #################################################################
            echo "<p> <font size= 4> c. Idioma del navegador (cliente) </font></p>";
            echo "<b>". $_SERVER['HTTP_ACCEPT_LANGUAGE']. "</b> <br /> <br />";
        ?>
    </body>
</html>