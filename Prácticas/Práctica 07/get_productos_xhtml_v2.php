<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<?php
		$data = array();

		if(isset($_GET['tope'])){
				$tope = $_GET['tope'];
		}
		else{
			die('Parámetro "tope" no detectado...');
		}

		if (!empty($tope)){
			/** SE CREA EL OBJETO DE CONEXION */
			@$link = new mysqli('localhost', 'root', 'akuatsu1221', 'marketzone');
			/** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

			/** comprobar la conexión */
			if ($link->connect_errno) {
				die('Falló la conexión: '.$link->connect_error.'<br/>');
				//exit();
			}
		#######################################################################

		# TIPO DE CONSULTA 
			/** Crear una tabla que no devuelve un conjunto de resultados */

			if ( $result = $link->query("SELECT * FROM productos WHERE unidades <= $tope") ) {
					
				/** Se extraen las tuplas obtenidas de la consulta */
				$row = $result->fetch_all(MYSQLI_ASSOC);

				/** Se crea un arreglo con la estructura deseada */
				foreach($row as $num => $registro) {            // Se recorren tuplas
					foreach($registro as $key => $value) {      // Se recorren campos
						$data[$num][$key] = utf8_encode($value);
					}
				}
					
				/** útil para liberar memoria asociada a un resultado con demasiada información */
				$result->free();
			}
			$link->close();
		}
	?>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset= UTF-8" />
		<title>Productos</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
				integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
				crossorigin="anonymous">
		<script>
			function show() {
				// se obtiene el id de la fila donde está el botón presinado
				var rowId = event.target.parentNode.parentNode.id;

				// se obtienen los datos de la fila en forma de arreglo
				var data = document.getElementById(rowId).querySelectorAll(".row-data");
				/**
				querySelectorAll() devuelve una lista de elementos (NodeList) que 
				coinciden con el grupo de selectores CSS indicados.
				(ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

				En este caso se obtienen todos los datos de la fila con el id encontrado
				y que pertenecen a la clase "row-data".
				*/

            	var nombre 		= data[0].innerHTML;
				var marca 		= data[1].innerHTML;
				var modelo 		= data[2].innerHTML;
				var precio 		= data[3].innerHTML;
				var unidades	= data[4].innerHTML;
				var detalles 	= data[5].innerHTML;
				var imagen 		= data[6].firstChild.getAttribute('src');


                alert("id: " + rowId + "\nNombre: " + nombre + "\nMarca: " + marca + "\nModelo: " + modelo + "\nPrecio: " + precio + "\nUnidades: " + unidades + "\nDetalles: " + detalles + "\nImagen: " + imagen);

                send2form(rowId, nombre, marca, modelo, precio, unidades, detalles, imagen);
            }
        </script>
	</head>

	<body>
		<h3>PRODUCTO</h3>

		<br/>
			<table class="table">
		<!--	Encabezado		-->
				<thead class="thead-dark">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
					<th scope="col">Modificar datos</th>
					</tr>
				</thead>
				<tbody>
		<!--	Filas de la tabla		-->
				<?php foreach($data as $array => $indice){ ?>
					<tr id = <?= $indice['id'] ?>>
						<th scope="row"><?= $indice['id'] ?></th>
						<td class="row-data"><?= $indice['nombre'] ?></td>
						<td class="row-data"><?= $indice['marca'] ?></td>
						<td class="row-data"><?= $indice['modelo'] ?></td>
						<td class="row-data"><?= $indice['precio'] ?></td>
						<td class="row-data"><?= $indice['unidades'] ?></td>
						<td class="row-data"><?= utf8_encode($indice['detalles']) ?></td>
						<td class="row-data"><img src=<?= $indice['imagen'] ?> ></td>
						<td class="row-data"><input type="button" 
									value="Modificar" 
									onclick="show()" /></td>
					</tr>
				</tbody>
				<?php  } ?>
			</table>
			
			<script> 
				function send2form(rowId, nombre, marca, modelo, precio, unidades, detalles, imagen){    //form) { 
					var urlForm = "http://localhost/tecnologiasweb/practicas/p07/formulario_productos_v2.php";
					var propId = "rowId="+rowId;
					var propNombre = "nombre="+nombre;
					var propMarca = "marca="+marca;
					var propModelo 	= "modelo="+modelo;
					var propPrecio 	= "precio="+precio;
					var propUnidades = "unidades="+unidades;
					var propDetalles = "detalles="+detalles;
					var propImagen = "imagen="+imagen;

					window.open(urlForm+"?"+propId+"&"+propNombre+"&"+propMarca+"&"+propModelo+"&"+propPrecio+"&"+propUnidades+"&"+propDetalles+"&"+propImagen);
				}
			</script>
	</body>
</html>