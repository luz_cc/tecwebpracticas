<!DOCTYPE html >
<html>
    <head>
        <meta charset="utf-8" >
        <title>Actualización de datos</title>
        <style type="text/css">
            ol, ul { 
                list-style-type: none;
            }
        </style>

        <script type="text/javascript">

            function validar(){
                var nombre  = document.getElementById('form-name').value;
                var marca   = document.getElementById('form-marca').value;
                var modelo  = document.getElementById('form-model').value;
                var precio  = document.getElementById('form-price').value;
                var detalles = document.getElementById('form-details').value;
                var unidades = document.getElementById('form-units').value;
                var imagen  = document.getElementById('form-img').value;


                var alfanumerico = /^[A-Za-z0-9\-\ ]+$/g;
                var decimal =  /^\d*(\.\d{3})?\d{0,1}$/;
                var extension = /\.(jpg|png)$/i;

                var valido = true;

            /** PARTE "Nombre" 
                * El nombre debe ser requerido. */
                if(!nombre || nombre.length <= 4){
                    alert(' Nombre del producto no dado o muy corto.');
                    valido = false;
                /** El nombre debe tener 100 caracteres o menos. */
                } else if(nombre.length > 100){
                    alert(' Nombre del producto muy largo.');
                    valido = false;
                }

            /** PARTE "Marca" 
                * La marca debe ser requerida y seleccionarse de una lista de opciones. */
                if(marca == ''){
                    alert('Ninguna marca seleccionada.');
                    valido = false;
                }

            /** PARTE "Modelo" 
                * El modelo debe ser requerido. */
                if(!modelo || modelo.length <= 4){
                    alert('Nombre del modelo no dado o muy corto');
                    valido = false;
                /** El modelo debe tener 25 caracteres o menos. */
                } else if(modelo.length > 25){
                    alert('Nombre del modelo muy largo');
                    valido = false;
                /** El modelo debe ser tipo texto alfanumérico.
                    Se permite el guión (-) y el espacio en blanco ( ) */
                } else if(!alfanumerico.test(modelo)){
                    alert('El nombre del modelo con caracteres invalidos (no alfanuméricos).');
                    valido = false;
                } 

            /** PARTE "Precio" 
             * El precio debe ser requerido y debe ser mayor a 99.99*/
                if(!precio){
                    alert('Precio del producto no dado.');
                    valido = false;
                } else if(precio <= 99.99){
                    alert('El precio debe ser mayor a 99.99');
                    valido = false;
                /** Se valida que el precio tenga el punto decimal (.) */
                } else if(decimal.test(precio)){
                    alert('El precio debe tener un " . " p.e: 200.49 ');
                    valido = false;
                }

            /** PARTE "Detalles" 
             * Los detalles del producto son opcionales. 
             * De usarse, deben tener 250 caracteres o menos.*/
                if(detalles.length > 250){
                    alert('Detalles del producto muy extensos (máximo 250 caracteres).');
                    valido = false;
                }

            /** PARTE "Unidades" 
             * Las unidades deben ser requeridas y el número registrado debe ser mayor o igual a 0 */
                if(!unidades){
                    alert('Número de unidades disponibles del producto no dado.');
                    valido = false;
                }else if(unidades < 0){
                    alert('El número de unidades disponibles del producto debe ser igual o mayor a 0.');
                    valido = false;
                }

            /** PARTE "Imagen" 
             * La ruta de la imagen es opcional.
             * En caso de no registrarse se debe usar la ruta de una imagen por defecto  */
                if(!imagen){
                    imagen.value = "img/imagen_ejem.png";
                } else if(!extension.test(imagen)) {
                    alert('El archivo a adjuntar no es una imagen');
                    valido = false;
                }

                return valido;
            }

            function cambio(){
                var seleccion=document.getElementById('form-marca');
                document.getElementById('contenido').value=seleccion.options[seleccion.selectedIndex].text;
			}
        </script>
    </head>

    <body>
        <h1 style="text-align:center;">&ldquo;Marketzone&rdquo;</h1>
        <h1 style="text-align:center;">Datos de Producto</h1>
    
        <form id="formulario" action="http://localhost/tecnologiasweb/practicas/p07/formulario_productos_v3.php" 
                method="post" onsubmit='return validar()'>

            <h2>Información general</h2>

            <fieldset>
                <legend>Modifica los datos de este producto: </legend>
                <ul>

                 <!--Parte "Id"--> 
                    <li><label>ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="hidden" name="id"
                            value="<?= !empty($_POST['rowId'])?$_POST['rowId']:$_GET['rowId'] ?>">

                            <strong>
                                <label>&nbsp;<?= !empty($_POST['rowId'])?$_POST['rowId']:$_GET['rowId'] ?> </label>
                            </strong> 
                    </li>

                    <br>

                <!--Parte "Nombre"--> 
                    <li><label for="form-name">Nombre:</label>
                        <input type="text" name="name" id="form-name" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>">
                    </li>

                    <br>

                <!--Parte "Marca"--> 
                    <li><label for="form-marca">Marca:</label>
                        <select name="form-marca" id="form-marca" onChange="cambio()">
                            <option value="">Selcciona una opción</option>
                                <option value="1">V MODA</option> 
                                <option value="2">Sennheiser</option> 
                                <option value="3">Bose</option>
                                <option value="4">Audio Technica</option> 
                                <option value="5">Panasonic</option> 
                                <option value="6">Denon</option> 
                                <option value="7">Shure</option> 
                                <option value="8">Shure</option>
                                <option value="9">AKG</option>
                                <option value="10">SONY</option>
                        </select>

                        <label style="font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marca actual:</label>
                        <strong>
                            <label>&nbsp;&nbsp;<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?> </label>
                        </strong> 

                        <!--Marca nueva--> 
                        <input type="text" id="contenido" name="marcaC" hidden>

                    </li>
                    

                    <br>
                    
                <!--Parte "Modelo"--> 
                    <li><label for="form-model">Modelo:</label>
                        <input type="text" name="model" id="form-model"
                            value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>">
                    </li>

                    <br>

                <!--Parte "Precio"-->                     
                    <li><label for="form-price">Precio: ($)</label>
                        <input step="any" type="number" name="price" id="form-price"
                            value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>">
                    </li>

                    <br>

                <!--Parte "Detalles"--> 
                    <li><label for="form-details">Detalles del producto:</label><br>
                        <textarea name="details" rows="4" cols="60" id="form-details"><?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?></textarea>
                    </li>

                    <br>

                <!--Parte "Unidades"--> 
                    <li><label for="form-units">Unidades:</label>
                        <input type="number" name="units" id="form-units"
                            value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>">
                    </li>
                    
                    <br>

                <!--Parte "Imagen"--> 
                    <li><label for="form-img">Imagen del producto(ruta): </label>
                        <input type="text" name="img" id="form-img" 
                            value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>">
                    </li>

                    <br>
                </ul>

            </fieldset>

            <p>
                <input type="submit" value="Actualizar" id="enviar">
                <input type="reset">
            </p>

        </form>
        <h3>Elaborado por: <br> Cota Cagal Lucero </h3>
    
        <script> 
			function send2form(marcaC){
				var form = document.createElement("form");

			/**Parte "Marca" */
				var marcaCIn = document.createElement("input");
				marcaCIn.type = 'text';
				marcaCIn.name = 'marcaC';
				marcaCIn.value = marcaC;
				form.appendChild(marcaCIn);
				

				console.log(form);

				form.method = 'POST';
				form.action = 'http://localhost/tecnologiasweb/practicas/p07/formulario_productos_v3.php';  

				document.body.appendChild(form);
				form.submit();
			}
		</script>
    </body>
</html>