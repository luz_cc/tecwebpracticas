<!DOCTYPE html >
<html>
    <head>
        <meta charset="utf-8" >
        <title>Actualización de datos</title>
        <style type="text/css">
            ol, ul { 
                list-style-type: none;
            }
        </style>

        <?php
            $id          = $_POST['id'];
            $nombre      =$_POST['name'];
            $marca       = $_POST['marcaC'];
            $modelo      = $_POST['model'];
            $precio      = $_POST['price'];
            $detalles    = $_POST['details'];
            $unidades    = $_POST['units'];
            $imagen      = $_POST['img'];

            /** SE CREA EL OBJETO DE CONEXION */
            @$link = new mysqli('localhost', 'root', 'akuatsu1221', 'marketzone');	

            /** comprobar la conexión */
            if ($link->connect_errno) {
                die('Falló la conexión: '.$link->connect_error.'<br/>');
                /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
            }
            else{
               // echo "Conexion exitosa. <br>";
            }

            $select = "SELECT * FROM productos WHERE id = '$id'";

            if($link->query($select)){
              //  echo "<br>Seleccionado el elemento de ID: ". $id;

                $up = "UPDATE productos SET nombre= '$nombre', marca= '$marca', modelo= '$modelo', precio= '$precio', detalles= '$detalles', unidades= '$unidades', imagen= '$imagen' WHERE id= $id";

                if($link->query($up)){
                    echo "<script> alert('Datos actualizados') </script>";
                }
                else{
                    echo "<script> alert('Los datos del producto no pudieron ser actualizados =(') </script>";
                }
            }
            else{
                echo "Error";
            }

            $link->close();
        ?>
    </head>

    <body>
    
        <form id="formulario" method="post">

            <h2>Información general</h2>

            <fieldset>
                <legend>Resumen de datos de producto actualizado: </legend>
                <ul>

                 <!--Parte "Id"--> 
                    <li><label>ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="text" name="id" style="text-align: center; width : 50px;" disabled
                            value="<?= !empty($_POST['id'])?$_POST['id']:$_GET['id']?>">
                    </li>

                    <br>

                <!--Parte "Nombre"--> 
                    <li><label for="form-name">Nombre:</label>
                        <input type="text" name="name" id="form-name" disabled
                            value="<?= !empty($_POST['name'])?$_POST['name']:$_GET['name'] ?>">
                    </li>

                    <br>

                <!--Parte "Marca"--> 
                    <li><label for="form-marca">Marca: </label>
                            <input type="text" name="marcaC" id="form-marca" disabled
                            value="<?= !empty($_POST['marcaC'])?$_POST['marcaC']:$_GET['marcaC'] ?>">
                    </li>

                    <br>
                    
                <!--Parte "Modelo"--> 
                    <li><label for="form-model">Modelo:</label>
                        <input type="text" name="model" id="form-model" disabled
                            value="<?= !empty($_POST['model'])?$_POST['model']:$_GET['model'] ?>">
                    </li>

                    <br>

                <!--Parte "Precio"-->                     
                    <li><label for="form-price">Precio: ($)</label>
                        <input step="any" type="number" name="price" id="form-price" disabled
                            value="<?= !empty($_POST['price'])?$_POST['price']:$_GET['price'] ?>">
                    </li>

                    <br>

                <!--Parte "Detalles"--> 
                    <li><label for="form-details">Detalles del producto:</label><br>
                        <textarea name="details" rows="4" cols="60" id="form-details" disabled ><?= !empty($_POST['details'])?$_POST['details']:$_GET['details'] ?></textarea>
                    </li>

                    <br>

                <!--Parte "Unidades"--> 
                    <li><label for="form-units">Unidades:</label>
                        <input type="number" name="units" id="form-units" disabled
                            value="<?= !empty($_POST['units'])?$_POST['units']:$_GET['units'] ?>">
                    </li>
                    
                    <br>

                <!--Parte "Imagen"--> 
                    <li><label for="form-img">Imagen del producto(ruta): </label>
                        <input type="text" name="img" id="form-img" disabled
                            value="<?= !empty($_POST['img'])?$_POST['img']:$_GET['img'] ?>">
                    </li>

                    <br>
                </ul>

            </fieldset>

        </form>

        <h3>Elaborado por: <br> Cota Cagal Lucero </h3>
    
    </body>
</html>