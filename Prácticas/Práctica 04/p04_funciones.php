<?php
#################################################################
    // Ejercicio 1
    function multiplo($numero){
        if (($numero%5 == 0)  and ($numero%7 == 0)){
            echo $numero.' es múltiplo de 5 y de 7.';
        }
        else{
            echo $numero.' <b>NO</b> múltiplo de 5 y de 7.';
        }
    }

#################################################################
    // Ejercicio 2
    function create_matriz(){

        $matriz = array();
        
          $num1= rand(100,999);
          $num2= rand(100,999);
          $num3= rand(100,999);
          global $i;
    
          while(true){
          
              if($num1%2 != 0 and $num2%2 == 0 and $num3%2 !=0){
                $matriz[$i] = array($num1,$num2,$num3);
                break;
            }
          
                  $matriz[$i] = array($num1,$num2,$num3);
     
                
                $num1= rand(100,999);
                  $num2= rand(100,999);
                  $num3= rand(100,999);
            $i += 1;
        }
        
        return $matriz;
    }

#################################################################
    // Ejercicio 3 Ciclo While
    function primer_multiploW($numero){ 
        $multiplo = rand($numero, 1000);
        $i = 1;
        while( $multiplo%$numero !=0 ){
            $i += 1;
            $multiplo = rand($numero,1000);
        }
        echo "Iteraciones realizadas: ". $i . "<br />";
        return $multiplo;
    }

    // Ejercicio 3 Ciclo Do While
    function primer_multiploDW($num){ 
        $multiplo2 = rand($num, 1000);
        $j = 1;
        
        do{
            $j += 1;
            $multiplo2 = rand($num,1000);

        }while($multiplo2%$num !=0);

        echo "Iteraciones realizadas: ". $j . "<br />";
        return $multiplo2;
    }

#################################################################
    // Ejercicio 4
    function create_array(){
        $aux2 = array();
        
        for($i = 97; $i<=122; $i++){
            $aux2[$i] = chr($i);
        }
        return $aux2;
      }

?>