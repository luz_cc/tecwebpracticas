<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> Práctica 4 LCC</title>
    </head>

    <body>
        <h2><p style="text-align:center"> Práctica 4 - Uso de funciones, ciclos y arreglos en PHP </p></h2>
        <h3> Elaborado por: <br />
            Cota Cagal Lucero
        </h3>
        <?php
        #################################################################

            echo "<h2> Ejercicio 1 </h2>";
            $numero = $_GET['numero'];
            /*localhost/tecnologiasweb/practicas/p04/p04_principal.php?numero=35*/
            include("p04_funciones.php");
            multiplo($numero);

        #################################################################

            echo "<h2> Ejercicio 2 </h2>";

            $i = 1;
            $triple = create_matriz();

            echo "<br>";

            foreach($triple as $indice => $fila){
                //echo "[$indice] => $dato<br>";
                foreach($fila as $indice => $nro){
                    echo $nro . " ";
                }
                echo "<br>";
            }

            $filas = count($triple);
            $elem = count($triple,COUNT_RECURSIVE); 
            
            $total = $elem-$filas;

            echo "<br>". $total . " números obtenidos  en " . $i . " iteracion(es).";
        
        #################################################################

            echo "<h2> Ejercicio 3 </h2>";
            echo "<h3> Ciclo While </h3>";

            $numero = $_GET['numero'];

            $primero = primer_multiploW($numero);
            echo "El primer múltiplo generado aleatoriamente de ". $numero. " es ". $primero."<br />";

            unset($numero);


            echo "<h3> Ciclo Do While </h3>";

            $numero = $_GET['numero'];

            $primero2 = primer_multiploDW($numero);
            echo "El primer múltiplo generado aleatoriamente de ". $numero. " es ". $primero2."<br />";

            unset($numero);


        #################################################################
            
            echo "<h2> Ejercicio 4 </h2>";

            $arreglo = create_array();

            foreach($arreglo as $indice => $dato){
                echo "[$indice] => $dato<br>";
            }

            unset($arreglo);

        #################################################################
        ?>
    </body>
</html>