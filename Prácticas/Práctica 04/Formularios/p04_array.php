<?php

function carro_array(){
    $carros = array();

    $carros ['UBN6338'] = array(
        "Auto"=>array(
            'Marca'=>"HONDA", 
            'Modelo'=>2020, 
            'Tipo'=>"camioneta"),
            
        "Propietario"=>array(
            'Nombre'=>"Alfonzo Esparza", 
            'Ciudad'=> "Puebla, Pue.", 
            'Direccción'=> "C.U., Jardines de San Manuel ")
    );
    
    $carros ['UBN6339'] = array(
        "Auto"=>array(
            'Marca'=>"MAZDA", 
            'Modelo'=>2019, 
            'Tipo'=>"sedan"),
            
        "Propietario"=>array(
            'Nombre'=>" Ma. del Consuelo Molina ", 
            'Ciudad'=> "Puebla, Pue.", 
            'Direccción'=> "97 oriente")

    );

    $carros ['UBN6340'] = array(
        "Auto"=>array(
            'Marca'=>"CHEVROLET", 
            'Modelo'=>2022, 
            'Tipo'=>"sedan"),
            
        "Propietario"=>array(
            'Nombre'=>" Brenda Martínez ", 
            'Ciudad'=> "Puebla, Pue.", 
            'Direccción'=> "102 oriente")

    );

    

//   echo('<pre>');
//   print_r($carros);
//   echo('<pre>');
return $carros;
}

?>