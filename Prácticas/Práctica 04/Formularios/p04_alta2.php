<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="css/formulario1.css">

        <!--Etiquetas para eliminar la caché-->
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">

        <title> Resultados </title>
    </head>

    <body>
        <div class="contenedor" style ="margin-top: 20px; padding-bottom: 20px; margin-bottom: 50px ">
            <div class="partSuperior">
                <h1><span>Resultados</span> de <span>búsqueda</span></h1>
            </div>

            <form class="formulario">
            <br />   <br />

            <div class = "texto">
                <?php
                    include("p04_array.php");

                    $array = carro_array();

                    $buscar = $_POST['matricula'];
                    $index = -1;

                    if($buscar){
                        echo "<h1>Búsqueda por matrícula</h1> <br />";
                        foreach($array as $indice => $detalles){
                            if ($indice == $buscar){
                                $index = 1;
                                echo "<h1> $indice </h1> <br />";
                                
                                foreach($detalles as $indice => $valor){
                                    echo "<p> <br /> <b> $indice </b> </p>";
    
                                    foreach($valor as $indice => $valores){
                                        echo "<p> $indice: $valores </p>";
                                    }
                                }
                                break;
                            }
                        }
                        if($index == -1){
                            echo "<h1><br>Ningún registro <br> encontrado</h1> <br />";
                        }
    

                    }
                    else{
                        echo "<h1>Todos los registros </h1> <br />";
                        foreach($array as $indice => $detalles){
                            echo "<h1> $indice </h1> <br />";
                                
                            foreach($detalles as $indice => $valor){
                                echo "<p> <br /> <b> $indice </b> </p>";
    
                                foreach($valor as $indice => $valores){
                                    echo "<p> $indice: $valores </p>";
                                }
                            }
                            echo '<br /> <br />';
                        }
                    } 
                ?>
             </div>

            </form>
        </div>
    </body>
</html>