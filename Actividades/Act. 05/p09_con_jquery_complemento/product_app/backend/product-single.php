<?php
    include_once __DIR__.'/database.php';
    $data = array();

    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
        if ($result = $conexion->query("SELECT * FROM productos WHERE id = '{$id}'") ) {
			$data = $result->fetch_array(MYSQLI_ASSOC);
			/** útil para liberar memoria asociada a un resultado con demasiada información */
            $result->free();
		}
		$conexion->close();
    } 
    
    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>