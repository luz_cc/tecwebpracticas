<?php
    include_once __DIR__.'/database.php';

    $producto = file_get_contents('php://input');
    $data = array(
        'status'  => 'error',
        'message' => 'No existe el producto en la base de datos'
    );

    // SE TRANSFORMA EL STRING DEL JASON A OBJETO
    $jsonOBJ = json_decode($producto);
        
    //Acceso a datos del producto
    $idP          = $jsonOBJ->id;
    $nombreP      = $jsonOBJ->nombre;
    $marcaP       = $jsonOBJ->marca;
    $modeloP      = $jsonOBJ->modelo;
    $precioP      = $jsonOBJ->precio;
    $detallesP    = $jsonOBJ->detalles;
    $unidadesP    = $jsonOBJ->unidades;
    $imagenP      = $jsonOBJ->imagen;

    $select = "SELECT * FROM productos WHERE id = '$idP'";

    if($conexion->query($select)){
        
        $up = "UPDATE productos SET nombre= '$nombreP', marca= '$marcaP', modelo= '$modeloP', precio= '$precioP', detalles= '$detallesP', unidades= '$unidadesP', imagen= '$imagenP' WHERE id= $idP";
        if($conexion->query($up)){
            $data['status'] =  "success";
            $data['message'] =  "Datos actualizados";
        }
        else{
            $data['message'] = "ERROR: No se ejecutó $up. " . mysqli_error($conexion);
        }
    }
    $conexion->close();

    echo json_encode($data, JSON_PRETTY_PRINT);
?>