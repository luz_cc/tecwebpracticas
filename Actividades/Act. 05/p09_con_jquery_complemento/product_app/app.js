function cambio(){
    var seleccion = document.getElementById('form-marca');
    document.getElementById('productMarcaChange').value = seleccion.options[seleccion.selectedIndex].text;
}

$(document).ready(function(){
    let edit = false;
/**Se carga toda la lista de productos NO eliminados al abrir la página. */
    function mostrarLista(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function(response){
                let products = JSON.parse(response);
                let template = '';

                    products.forEach(product => {

                        let descripcion = '';
                        descripcion += '<li>precio: '+product.precio+'</li>';
                        descripcion += '<li>unidades: '+product.unidades+'</li>';
                        descripcion += '<li>modelo: '+product.modelo+'</li>';
                        descripcion += '<li>marca: '+product.marca+'</li>';
                        descripcion += '<li>detalles: '+product.detalles+'</li>';

                        template += `
                            <tr productId="${product.id}">
                                <td>${product.id}</td>
                                <td><a href="#" class = "product-item">${product.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td><button class="product-delete btn btn-danger"> Eliminar</button></td>
                            </tr>
                        `;
                    });
                $('#products').html(template);
            }
        })
    }
    // SE LISTAN TODOS LOS PRODUCTOS
    mostrarLista();

/** Cargar en la barra de estado los nombre de los productos NO eliminados coincidentes con lo que se 
 * “vaya” tecleando en el campo de búsqueda. */
    $('#product-result').hide();
    $('#search').keyup(function(e){
        if($('#search').val()){
            let search = $('#search').val();
            console.log(search);
            $.ajax({
                url: 'backend/product-search.php',
                type: 'POST',
                data: { search },
                success: function(response){

                    let products = JSON.parse(response);
                    let template = '';

                    let template_bar = '';
                    products.forEach(product => {
                         template_bar += `
                            <li>${product.nombre}</li>
                        `;
        /**Cargar en la tabla de productos NO eliminados que coincidan con lo que se “vaya” tecleando en el campo de búsqueda. */
                        let descripcion = '';
                        descripcion += '<li>precio: '+product.precio+'</li>';
                        descripcion += '<li>unidades: '+product.unidades+'</li>';
                        descripcion += '<li>modelo: '+product.modelo+'</li>';
                        descripcion += '<li>marca: '+product.marca+'</li>';
                        descripcion += '<li>detalles: '+product.detalles+'</li>';

                        template += `
                            <tr productId="${product.id}">
                                <td>${product.id}</td>
                                <td><a href="#" class = "product-item">${product.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td><button class="product-delete btn btn-danger"> Eliminar</button></td>
                            </tr>
                        `;
                    });
                $('#container').html(template_bar);
                $('#products').html(template);
                $('#product-result').slideDown("slow");
                }
            })
        }else{
            $('#product-result').slideUp("slow");
        }
    })

    $('#validation-result').hide();
    var template_val = '';
/** PARTE "Nombre". El nombre debe tener 100 caracteres o menos. */
    $('#name').focusout(function(e){
        if($('#name').val() && $('#name').val().length <= 4){
            template_val = ` <li>Nombre del producto muy corto.</li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        }
        else if($('#name').val().length > 100){
            template_val = ` <li>Nombre del producto muy largo.</li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });

/** PARTE "Marca". La marca debe seleccionarse de una lista de opciones. */
    $('#form-marca').focusout(function(e){
        if($('#form-marca').val() == ""){
            template_val = ` <li>Ninguna marca seleccionada.</li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });

/** PARTE "Modelo". El modelo debe ser tipo texto alfanumérico. Se permite el guión (-) y el espacio en blanco ( ) */
    $('#productModel').focusout(function(e){
        let alfanumerico = /^[A-Za-z0-9\-\ ]+$/g;
        if($('#productModel').val() && !alfanumerico.test($('#productModel').val())){
            template_val = ` <li>El nombre del modelo contiene caracteres no validos.</li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        }
        /** El modelo debe tener 25 caracteres o menos. */
        else if($('#productModel').val().length > 25){
            template_val = ` <li>Nombre del modelo muy largo. </li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });

/** PARTE "Precio". El precio debe ser mayor a 99.99*/
    $('#productPrice').focusout(function(e){
        let decimal =  /^\d*(\.\d{3})?\d{0,1}$/;
        if($('#productPrice').val() && $('#productPrice').val() <= 99.99){
            template_val = ` <li>El precio debe ser mayor a 99.99 </li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
    /** Se valida que el precio tenga el punto decimal (.) */
        } else if($('#productPrice').val() && decimal.test($('#productPrice').val())){
            template_val = ` <li>El precio debe tener un " . " p.e: 200.49 </li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });

/** PARTE "Detalles".  De usarse, debe tener menos de 250 caracteres.*/
    $('#productDetails').focusout(function(e){
        if($('#productDetails').val().length > 250){
            template_val = ` <li>Detalles del producto muy extensos. </li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });

/** PARTE "Unidades". El número registrado debe ser mayor o igual a 0 */
    $('#productUnits').focusout(function(e){
        if($('#productUnits').val() && $('#productUnits').val() < 0){
            template_val = ` <li>El número de unidades disponibles del producto debe ser igual o mayor a 0</li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });

/** PARTE "Imagen". En caso de no registrarse se debe usar la ruta de una imagen por defecto  */
    $('#productImg').focusout(function(e){
        let extension = /\.(jpg|png)$/i;
        if($('#productImg').val() && !extension.test($('#productImg').val())){
            template_val = ` <li>El archivo a adjuntar no es una imagen. </li> `;
            $('#containerVal').html(template_val);
            $('#validation-result').show("slow");
        } else {
            $('#validation-result').hide("slow");
        }
    });


    $('#product-form').submit(function(e){
        e.preventDefault();
/**Validar que los campos requeridos no estén vacíos antes de "agregar el producto" a la BD */
        function validar(){
            let valido = true;
            let template_val = '';

            if(!$('#name').val()){  /** El nombre debe ser requerido. */
                template_val += ` <li>Nombre del producto no dado.</li> `;
                $('#containerVal').html(template_val);
                valido = false;
            }

            if(!$('#form-marca').val()){  /** La marca debe ser requerida. */
                template_val += ` <li>Ninguna marca seleccionada.</li> `;
                $('#containerVal').html(template_val);
                valido = false;
            }

            if(!$('#productModel').val()){  /**El modelo debe ser requerido */
                template_val += ` <li>Modelo del producto no dado.</li> `;
                $('#containerVal').html(template_val);
                valido = false;
            }

            if(!$('#productPrice').val()){  /**El precio debe ser requerido */
                template_val += ` <li>Precio del producto no dado.</li> `;
                $('#containerVal').html(template_val);
                valido = false;
            }

            if(!$('#productDetails').val()){  /**Detalles opcionales */
                $('#productDetails').val("N/A");
            }

            if(!$('#productUnits').val()){  /**Las unidades deben ser requeridas. */
                template_val += ` <li>Número de unidades disponibles del producto no dado.</li> `;
                $('#containerVal').html(template_val);
                valido = false;
            }

            if(!$('#productImg').val()){  /**La ruta de la imagen es opcional. */
                $('#productImg').val("img/imagen_ejem.png");
            }

            if(valido == false){
                $('#validation-result').show("slow");
            }

            $('input').click(function(e){
                $('#validation-result').hide("slow");
            });

            return valido;
        }

        let url =  edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
        console.log(url);

        if(validar()){
            $('#product-result').hide();

            const postData = {
                id: $('#productId').val(),
                eliminado: $('#productEli').val(),
                nombre: $('#name').val(),
                marca: $('#productMarcaChange').val(),
                modelo: $('#productModel').val(),
                precio: $('#productPrice').val(),
                detalles: $('#productDetails').val(),
                unidades: $('#productUnits').val(),
                imagen: $('#productImg').val()
            }

            var productoJsonString = JSON.stringify(postData,null,2);
            console.log(productoJsonString);    

            $.post(url, productoJsonString, function(response){
                console.log(response);
                let mensajes = JSON.parse(response);
                let template_bar = '';

                    template_bar += `
                        <li style="list-style: none;">status: ${mensajes.status}</li>
                        <li style="list-style: none;">message: ${mensajes.message}</li>
                    `;
                $('#container').html(template_bar);
                $('#product-result').slideDown("slow");
    /**Cargar toda la lista de productos NO eliminados al presionar el botón de “Agregar Producto”, 
     * esto para visualizar inmediatamente la lista actualizada */
                mostrarLista();
                $('#product-result').slideUp(5000);
            });
        }
    });

/**Cargar toda la lista de productos NO eliminados después presionar el botón “Eliminar”, esto para visualizar 
 * inmediatamente la lista actualizada. */
    $(document).on('click', '.product-delete', function(){
        if(confirm('¿Desea eliminar el producto seleccionad?')){
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');

            $.post('backend/product-delete.php', {id}, function(response){
                let mensajes = JSON.parse(response);
                    let template_bar = '';

                    template_bar += `
                        <li style="list-style: none;">status: ${mensajes.status}</li>
                        <li style="list-style: none;">message: ${mensajes.message}</li>
                    `;
                    $('#container').html(template_bar);
                    $('#product-result').slideDown("slow");
               mostrarLista();
               $('#product-result').slideUp(5000);
            });
        }
    });

/** Funcionalidad de “edición”/“actualización”   */
    $('#marca-actual').hide();  
    $(document).on('click', '.product-item', function(){
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        
        $.post('backend/product-single.php', {id}, function(response){
            const aux = JSON.parse(response);   //Se convierte el resultado en un string

            //Se llenan los input del formulario
            $('#productId').val(aux.id);
            $('#productEli').val(aux.eliminado);
            $('#name').val(aux.nombre);
            $('#productModel').val(aux.modelo);
            $('#productPrice').val(aux.precio);
            $('#productDetails').val(aux.detalles);
            $('#productUnits').val(aux.unidades);
            $('#productImg').val(aux.imagen);
            $('#productMarca').val(aux.marca);

            edit = true;
        })
        $('#marca-actual').slideDown("slow");    //Se hace visible el campo de "Marca actual"
    });

/**Al teclear el nombre del producto, se debe hacer una conexión asíncrona al servidor para  validar que el 
 * nombre no exista en la BD. En caso de que el nombre del producto ya exista, deberá mostrarse un mensaje 
 * alusivo. */
     $('#name').keyup(function(e){
        let espera;     //variable para almacenar el tiempo de espera
        clearInterval(espera);  //Se limpia el intervalo de tiempo almacenado

        //Función que se ejecuta cuando el intervalo de tiempo se cumple
        espera = setInterval(function(){

            if($('#name').val()){
                let search = $('#name').val();
    
                $.ajax({
                    url: 'backend/product-search.php',
                    type: 'POST',
                    data: { search },
                    success: function(response){
    
                        let products = JSON.parse(response);
                        let aux;    //auxiliar
                        let template_bar = '';
    
                        products.forEach(product => {
                            //si se encuentra una coincidencia, se guarda el nombre
                            if(product.nombre.toLowerCase() == search.toLowerCase() ){
                                aux = product.nombre;   
                            }
                        })

                        //Se muestra un msj para la coincidencia
                        if(aux){
                            template_bar += `
                                <li style="list-style-type: none">Nombre existente en la base de datos</li>
                                <li>${aux}</li>
                            `;
                            $('#container').html(template_bar);
                            $('#product-result').slideDown("slow");
                        }else{
                            $('#product-result').slideUp("slow");
                        }
                    }
                })
            }
            clearInterval(espera);
        }, 1000); //Intervalo de tiempo en milisegundos(1 segundo)
    })
});